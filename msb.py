
## This file is where you add hook-ins for new languages. 
## You need to add to [get_reg_item, set_reg_item, inline_replace]. 
## The function 'inline_replace' is for subscripting.

def set_reg_item(language, varlist, tab=''):
	# Bash Shell
	if language == "sh":
		extracode = "\njfile=$(cat msb_register.json)"
		for var in varlist:
			extracode += "\njfile=$(echo $jfile | sed \"s/\(\\\"" + var + "\\\": \).*\(,.*\)/\\1${" + var + "}\\2/g\")"
		extracode += "\necho $jfile > msb_register.json"
		return extracode

	# Python
	if language == "python":
		extracode = "\n" + tab + "# Open json register"
		extracode += "\n" + tab + "import json"
		extracode += "\n" + tab + "with open('msb_register.json', 'r') as register:"
		extracode += "\n" + tab + "\tmsb_data = json.load(register)"
		# Iterate through list of variables
		extracode += "\n" + tab + "entry = {"
		for var in varlist:
			extracode += "\"" + var + "\": " + var + ","
		extracode = extracode[:len(extracode)-1] + "}"
		extracode += "\n" + tab + "msb_data.update(entry)"
		extracode += "\n" + tab + "with open('msb_register.json', 'w') as register:"
		extracode += "\n" + tab + "\tjson.dump(msb_data, register)"
		return extracode
		
	# Common Lisp
	if language == "clisp":
		extracode = """\n(defun json-set (var val json)
\t(setf val (if (integerp val)
\t\t(format nil "~a" val)
\t\t(format nil "\\\"~a\\\"" val)))
\t(setf end (+ (search var json) (length var) 3))
\t(setf start (position #\, json :start end))
\t(if (null start) (setf start (position #\} json :start end)))
\t(concatenate 'string (subseq json 0 end) val (subseq json start (length json))))

(let ((in (open "msb_register.json")))
\t(setf msb-data (read-line in))
\t\t(close in))"""
		# Iterate through list of variables
		for var in varlist:
			extracode += "\n(setf msb-data (json-set \""+ var + "\" "+ var + " msb-data))"
		extracode += """\n(with-open-file (str "msb_register.json"
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
(format str "~a" msb-data))"""
		return extracode
		
def get_reg_item(language, varlist, tab=''):
	# Bash Shell
	if language == "sh":
		extracode = "\n# Open json register"
		extracode += "\n" + tab + "jfile=$(cat \"msb_register.json\")"
		for var in varlist:
			extracode += "\n" + tab + var + "=`echo $jfile | sed 's/.*\\\"" + var + "\\\": \(.*\)\(\,\|\}\).*/\\1/g' | cut -d, -f1`"
		return extracode
		
	# Python 2.7
	if language == "python":
		extracode = "\n" + tab + "# Open json register"
		extracode += "\n" + tab + "import json"
		extracode += "\n" + tab + "with open('msb_register.json', 'r') as register:"
		extracode += "\n" + tab + "\tmsb_data = json.load(register)"
		# Iterate through list of variables
		for var in varlist:
			extracode += "\n"+ tab + var + " = msb_data[\'" + var + "\']"
		return extracode
		
	# Common Lisp
	if language == "clisp":
		extracode = """\n(defun json-get (var json)
\t(setf start (+ (search var json) (length var) 3))
\t(setf end (position #\, json :start start))
\t(if (null end) (setf end (position #\} json :start start)))
\t(setf value (subseq json start end))
\t(cond
\t\t((equal (char value 0) #\\\") (subseq value 1 (1- (length value))))
\t\t((equal (char value 0) #\\t) t)
\t\t((equal (char value 0) #\\f) nil)
\t\t (t (parse-integer value))))

(let ((in (open "msb_register.json")))
\t(setf msb-data (read-line in))
\t\t(close in))"""
		for var in varlist:
			extracode += "\n(setf " + var + "(json-get \"" + var + "\" msb-data))"
		return extracode
	
def inline_replace(language, subscript, tab=''):
	# Bash Shell
	if language == "sh":
		return "\n./" + subscript
		
	# Python 2.7
	if language == "python":
		return "\n" + tab + "from subprocess import call; call(\"./" + subscript + "\", shell=True)"
		
	# Common Lisp
	if language == "clisp":
		print "I'm so sorry :("
		return "; Nope"
