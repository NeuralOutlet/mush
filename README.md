# MuSh
### A simple meta-scripting language for parsing _multiple shebang_ statements that invoke different interpretors.

---

Although there is no real reason to do this you might want to just for the fun of it. Mush is python based and has a very simple approach, it splits up the script into smaller single-language scripts then runs them one after the other. Variables are transported between scripts using a register file (_msb\_registor.json_) and hook-in functions.

There is one important syntax for separating languages:

*(please ignore the '\\' before '\#', that's markdown hating me)*

``` \#!! <interpretor> <passed-variables ...> ```

The variables after the interpretor are ones that exist in the previous script and are used in the next one. When mush splits up the script it adds code for passing to / getting from the register - these are the hook-in functions. Adding an interpreted language to mush just needs these hook-ins to be written.

Below is a mush script using bash, python, and lisp. It can be found in _examples/guess_my_num.py_:

```
\#!/usr/bin/env mush

\#!! sh
\# Here we have the shell script prompting user for a num
read -p "Guess a number (0-10): " num
num=${num:-0}

\#!! python num
\# Then we pass the num to python where we
\# generate another number
import random
def pick_num():
	return random.randint(0, 10)

\# Then check for equality
mynum = pick_num()
print "my number: " + str(mynum)
print "Your number: " + str(num)
same = (num == mynum)

\#!! clisp same
; Finally we arrive in lisp to
; say the good news (hopefully)
(if (equal same "True")
	(format t "~C~a" #\linefeed "I guessed your number!")
	(format t "~C~a" #\linefeed "Damn, you win!"))
```

Using the vim syntax file, _editor\_support/mush.vim_, this looks quite lovely.